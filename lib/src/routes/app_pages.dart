import 'package:flutter_sfu2/src/app.dart';
import 'package:flutter_sfu2/src/pages/home/permession.dart';
import 'package:get/get.dart';
part 'app_routes.dart';

// ignore: avoid_classes_with_only_static_members
class AppPages {
  static const INITIAL = Routes.PERMESSION;

  static final routes = [
    GetPage(
      name: Routes.PERMESSION,
      page: () => PermissionHandlerWidget(),
      children: [],
    ),
  ];
}
