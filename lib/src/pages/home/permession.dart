import 'package:flutter/material.dart';
import 'package:flutter_sfu2/src/pages/home/home_page.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionHandlerWidget extends StatefulWidget {
  @override
  _PermissionHandlerWidgetState createState() =>
      _PermissionHandlerWidgetState();
}

class _PermissionHandlerWidgetState extends State<PermissionHandlerWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Permission Handler',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            _requestPermissions();
          },
          child: Text('Request Permissions'),
        ),
      ),
    );
  }

  Future<void> _requestPermissions() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.camera,
      Permission.microphone,
      // Add more permissions as needed
    ].request();

    // Check if all permissions are granted
    bool allPermissionsGranted =
        statuses.values.every((status) => status.isGranted);
    if (allPermissionsGranted) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomePage()),
      );
      // Navigate to another page
    } else {
      statuses.forEach((permission, status) {
        if (status.isDenied) {
          print('$permission permission is denied.');
        } else if (status.isPermanentlyDenied) {
          print('$permission permission is permanently denied.');
          // Handle if user denied the permission permanently
        }
      });
    }
  }
}
