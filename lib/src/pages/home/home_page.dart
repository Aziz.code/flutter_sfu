// ignore_for_file: avoid_print

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_sfu2/src/pages/home/widgets/custom_toast.dart';
import 'package:flutter_sfu2/src/pages/home/widgets/remote_view_card.dart';
import 'package:flutter_sfu2/src/services/socket_emit.dart';
import 'package:flutter_sfu2/src/shared/extensions/sdp_extention.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart' as RTC;
import 'package:flutter_webrtc/flutter_webrtc.dart';

import 'package:sdp_transform/sdp_transform.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

Map<String, dynamic> configuration = {
  'iceServers': [
    {
      "urls": "stun:stun.relay.metered.ca:80",
    },
    {
      "urls": "turn:standard.relay.metered.ca:80",
      "username": "86721531a8351f5134873628",
      "credential": "glTfzJOpzAiRVSBE",
    },
    {
      "urls": "turn:standard.relay.metered.ca:80?transport=tcp",
      "username": "86721531a8351f5134873628",
      "credential": "glTfzJOpzAiRVSBE",
    },
    {
      "urls": "turn:standard.relay.metered.ca:443",
      "username": "86721531a8351f5134873628",
      "credential": "glTfzJOpzAiRVSBE",
    },
    {
      "urls": "turns:standard.relay.metered.ca:443?transport=tcp",
      "username": "86721531a8351f5134873628",
      "credential": "glTfzJOpzAiRVSBE",
    }
  ],
  'iceTransportPolicy': 'all',
  'bundlePolicy': 'max-bundle',
  'rtcpMuxPolicy': 'require',
  'sdpSemantics': 'unified-plan',
};

Socket? socket;

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Map<String, dynamic>> socketIdRemotes = [];
  RTC.RTCPeerConnection? _peerConnection;
  RTC.MediaStream? _localStream;
  RTC.RTCVideoRenderer _localRenderer = RTC.RTCVideoRenderer();
  List<RTCIceCandidate> rtcIceCadidates = [];
  bool _isSend = false;
  bool _isFrontCamera = true;
  bool isAudioOn = true, isVideoOn = true;
  bool isConnected = false;
  @override
  void initState() {
    super.initState();
    initRenderers();
    // var urlConnectSocket = 'https://74cd-212-8-253-146.ngrok-free.app';
    // var urlConnectSocket = 'http://10.0.0.2:8000';
    var urlConnectSocket = 'https://sfu-pure.yorkbritishacademy.net/';
    socket = io(urlConnectSocket,
        OptionBuilder().enableForceNew().setTransports(['websocket']).build());
    socket!.connect();
    connectAndListen();
  }

  @override
  void dispose() {
    _peerConnection!.close();
    _localStream!.dispose();
    _localRenderer.dispose();
    socket!.disconnect();
    super.dispose();
  }

  _createPeerConnection() async {
    final Map<String, dynamic> offerSdpConstraints = {
      "mandatory": {
        "OfferToReceiveAudio": true,
        "OfferToReceiveVideo": true,
      },
      "optional": [],
    };

    RTC.RTCPeerConnection pc =
        await RTC.createPeerConnection(configuration, offerSdpConstraints);
    pc.onRenegotiationNeeded = () async {
      if (!_isSend) {
        _isSend = true;
        _createOffer();
      }
    };
    return pc;
  }

  _createPeerConnectionAnswer(socketId) async {
    print("pc before created");
    // final Map<String, dynamic> offerSdpConstraints = {
    //   "mandatory": {
    //     "OfferToReceiveAudio": true,
    //     "OfferToReceiveVideo": true,
    //   },
    //   "optional": [],
    // };
    RTC.RTCPeerConnection pc = await RTC.createPeerConnection(configuration);

    pc.onTrack = (track) {
      int index =
          socketIdRemotes.indexWhere((item) => item['socketId'] == socketId);
      if (track.streams.isNotEmpty) {
        socketIdRemotes[index]['stream'].srcObject = track.streams[0];
      }
    };
    pc.onRenegotiationNeeded = () async {
      _createOfferForReceive(socketId);
    };
    setState(() {});
    return pc;
  }

  void connectAndListen() async {
    try {
      socket!.onConnect((_) {
        _createPeerConnection().then(
          (pc) async {
            _peerConnection = pc;
            _localStream = await _getUserMedia();
            _localStream!.getTracks().forEach((track) {
              _peerConnection!.addTrack(track, _localStream!);
            });
            _peerConnection!.onConnectionState = (state) {
              _showToast(context, "peer connection state : ${state}");
              // if (state ==
              //     RTCPeerConnectionState.RTCPeerConnectionStateFailed) {
              //   _createOffer();
              // }
              print("peer connection state : ${state}");
            };
            _peerConnection!.onIceCandidate = (RTCIceCandidate candidate) {
              print(" on ice candidate event");
              print("zz candidate: ${candidate.candidate}");
              if (candidate.candidate != null) {
                SocketEmit().sendIceCandidateSender(candidate.candidate!,
                    socket!.id!, candidate.sdpMid, candidate.sdpMlineIndex);
                print("after function sending ");
              }
            };
          },
        );
        print('connected');
        print("Aziz_success");
        _showToast(context, 'Connected to server successfully');
        socket!.on('NEW-PEER-SSC', (data) async {
          // _showToast(context, 'NEW-PEER-SSC${data}');
          String newUser = data['socketId'];
          RTC.RTCVideoRenderer stream = RTC.RTCVideoRenderer();
          await stream.initialize();
          socketIdRemotes.add({
            'socketId': newUser,
            'pc': null,
            'stream': stream,
          });
          _createPeerConnectionAnswer(newUser).then((pcRemote) {
            print("socketId:::: ${newUser}");
            // pcRemote.onConnectionState = (state) {
            //   if (state ==
            //       RTCPeerConnectionState.RTCPeerConnectionStateFailed) {
            //     _createOfferForReceive(newUser);
            //   }
            // };
            socketIdRemotes[socketIdRemotes.length - 1]['pc'] = pcRemote;

            socketIdRemotes[socketIdRemotes.length - 1]['pc'].onIceCandidate =
                (candidate) async {
              if (candidate.candidate != null) {
                int index1 = socketIdRemotes
                    .indexWhere((element) => element['socketId'] == newUser);
                if (index1 != -1) {
                  print(
                      "inside index new on ice candidate in newPeer for :  ${newUser}");
                  SocketEmit().sendIceCandidateReceiver(candidate.candidate!,
                      newUser, candidate.sdpMid!, candidate.sdpMlineIndex);
                }
              } else {}
            };
            socketIdRemotes[socketIdRemotes.length - 1]['pc'].addTransceiver(
              kind: RTC.RTCRtpMediaType.RTCRtpMediaTypeVideo,
              init: RTC.RTCRtpTransceiverInit(
                direction: RTC.TransceiverDirection.RecvOnly,
              ),
            );
            socketIdRemotes[socketIdRemotes.length - 1]['pc'].addTransceiver(
              kind: RTC.RTCRtpMediaType.RTCRtpMediaTypeAudio,
              init: RTC.RTCRtpTransceiverInit(
                direction: RTC.TransceiverDirection.RecvOnly,
              ),
            );
          });

          setState(() {});
        });

        socket!.on('SEND-SSC', (data) async {
          // _showToast(context, 'SEND-SSC${data}');
          print('SEND-SSC');
          await _setRemoteDescription(data['sdp']);
          List<String> listSocketId = (data['sockets'] as List<dynamic>)
              .map((e) => e.toString())
              .toList();
          listSocketId.asMap().forEach((index, user) async {
            print("for_each : ${user} ");
            RTC.RTCVideoRenderer stream = RTC.RTCVideoRenderer();
            await stream.initialize();
            setState(() {
              socketIdRemotes.add({
                'socketId': user,
                'pc': null,
                'stream': stream,
              });
            });
            _createPeerConnectionAnswer(user).then((pcRemote) {
              socketIdRemotes[index]['pc'] = pcRemote;
              socketIdRemotes[index]['pc'].onIceCandidate = (candidate) async {
                if (candidate.candidate != null) {
                  int index1 = socketIdRemotes
                      .indexWhere((element) => element['socketId'] == user);
                  if (index1 != -1) {
                    print(
                        "inside index new on ice candidate in receive peer for :  ${user}");

                    SocketEmit().sendIceCandidateReceiver(candidate.candidate!,
                        user, candidate.sdpMid!, candidate.sdpMlineIndex);
                  }
                } else {}
              };
              socketIdRemotes[index]['pc'].addTransceiver(
                kind: RTC.RTCRtpMediaType.RTCRtpMediaTypeVideo,
                init: RTC.RTCRtpTransceiverInit(
                  direction: RTC.TransceiverDirection.RecvOnly,
                ),
              );

              socketIdRemotes[index]['pc'].addTransceiver(
                kind: RTC.RTCRtpMediaType.RTCRtpMediaTypeAudio,
                init: RTC.RTCRtpTransceiverInit(
                  direction: RTC.TransceiverDirection.RecvOnly,
                ),
              );
              socketIdRemotes[index]['pc'].onIceCandidate = (candidate) async {
                if (candidate.candidate != null) {
                  int index1 = socketIdRemotes
                      .indexWhere((element) => element['socketId'] == user);
                  if (index1 != -1) {
                    print(
                        "inside index new on ice candidate in receive peer for :  ${user}");

                    SocketEmit().sendIceCandidateReceiver(candidate.candidate!,
                        user, candidate.sdpMid!, candidate.sdpMlineIndex);
                  }
                } else {}
              };
            });
          });

          setState(() {});
        });

        socket!.on('RECEIVE-SSC', (data) async {
          // _showToast(context, 'RECEIVE-SSC${data}');
          int index = socketIdRemotes.indexWhere(
            (element) => element['socketId'] == data['socketId'],
          );
          if (index != -1) {
            await _setRemoteDescriptionForReceive(index, data['sdp']).then((_) {
              setState(() {});
            });
          }
        });
        socket!.on('SENDER-CANDIDATE', (data) async {
          // _showToast(context, "candidate for ${data['socketId']}");
          int index = socketIdRemotes.indexWhere(
            (element) => element['socketId'] == data['socketId'],
          );
          print("cadidateMy: ${data['candidate']}");
          if (data['candidate'] != null) {
            if (data['socketId'] == socket!.id) {
              print("before adding candidate");
              await _peerConnection!.addCandidate(RTCIceCandidate(
                  data['candidate'], data['sdpMid'], data['sdpMLineIndex']));
              print("after adding candidate");
            }
            if (index != -1) {
              if (socketIdRemotes[index]['pc'] != null) {
                socketIdRemotes[index]['pc'].addCandidate(RTCIceCandidate(
                    data['candidate'], data['sdpMid'], data['sdpMLineIndex']));
              }
            }
          }
          setState(() {});
        });
        socket!.on('RECEIVE-CANDIDATE', (data) async {
          // _showToast(context, "Receive candidate for ${data['socketId']}");
          int index = socketIdRemotes.indexWhere(
            (element) => element['socketId'] == data['socketId'],
          );
          print("cadidate: ${data['candidate']}");
          if (data['candidate'] != null) {
            if (data['socketId'] == socket!.id) {
              await _peerConnection!.addCandidate(RTCIceCandidate(
                  data['candidate'], data['sdpMid'], data['sdpMLineIndex']));
            }
            if (index != -1) {
              print("before adding candidate");
              if (socketIdRemotes[index]['pc'] != null) {
                await socketIdRemotes[index]['pc'].addCandidate(RTCIceCandidate(
                    data['candidate'], data['sdpMid'], data['sdpMLineIndex']));
                print("after adding candidate");
              }
            }
          }
          setState(() {});
        });

        // socket!.on('ICE-CANDIDATE', (data) async {
        //   // _showToast(context, "onIceCandidate ${data}");
        //   int index = socketIdRemotes.indexWhere(
        //     (element) => element['socketId'] == data['socketId'],
        //   );
        //   if (data['socketId'] == socket!.id) {
        //     // _showToast(context,
        //     //     " ice state before  ${_peerConnection!.iceConnectionState.toString()}");
        //     _showToast(context, "add candidate to my self");

        //     _peerConnection!
        //         .addCandidate(RTCIceCandidate(data['candidate'], "", 0));
        //     _showToast(context,
        //         " ice state After ${_peerConnection!.iceConnectionState}");
        //   }
        //   if (index != -1) {
        //     _showToast(context,
        //         " ice state before out ${socketIdRemotes[index]['pc'].iceConnectionState}");
        //     // _showToast(context, "add candidate to ${data['socketId']}");
        //     socketIdRemotes[index]['pc']
        //         .addCandidate(RTCIceCandidate(data['candidate'], "", 0));
        //     _showToast(context,
        //         " ice state After out ${socketIdRemotes[index]['pc'].iceConnectionState}");
        //   }
        //   setState(() {});
        // });

        socket!.on('user-disconnected', (data) async {
          setState(() {
            socketIdRemotes.removeWhere(
                (element) => element['socketId'] == data['userId']);
          });
        });
      });
      socket!.onError((error) {
        print("Aziz_error");
        print('Error connecting to the server: {$error}');
        _showToast(context, 'Error connecting to the server: $error');

        setState(() {});
      });
      socket!.onConnectError((data) {
        print('Connect Error: $data');
        _showToast(context, 'Connect Error: $data');
      });
      socket!.onConnectTimeout((data) {
        print('Connect Timeout: $data');
        _showToast(context, 'Connect Timeout: $data');
      });

      socket!.onDisconnect((_) {
        print('disconnect');
        socketIdRemotes
            .removeWhere((element) => element['socketId'] == socket!.id);
        _showToast(context, 'Disconnected from server');
      });
    } catch (error) {
      print("Error: $error");
      _showToast(context, 'Error: $error');
    }
  }

  initRenderers() async {
    await _localRenderer.initialize();
  }

  _setRemoteDescription(sdp) async {
    RTC.RTCSessionDescription description =
        RTC.RTCSessionDescription(sdp, 'answer');
    await _peerConnection!.setRemoteDescription(description);
  }

  _setRemoteDescriptionForReceive(indexSocket, sdp) async {
    RTC.RTCSessionDescription description =
        RTC.RTCSessionDescription(sdp, 'answer');
    RTC.RTCPeerConnection peer = socketIdRemotes[indexSocket]['pc'];
    await peer.setRemoteDescription(description);
    socketIdRemotes[indexSocket]['pc'] = peer;
    setState(() {});
  }

  _createOfferForReceive(String socketId) async {
    int index =
        socketIdRemotes.indexWhere((item) => item['socketId'] == socketId);
    if (index != -1) {
      RTC.RTCSessionDescription description =
          await socketIdRemotes[index]['pc'].createOffer({
        'offerToReceiveVideo': 1,
        'offerToReceiveAudio': 1,
      });
      socketIdRemotes[index]['pc'].setLocalDescription(description);
      var session = parse(description.sdp.toString());
      String sdp = write(session, null);
      await sendSdpOnlyReceive(sdp, socketId);
    }
    setState(() {});
  }

  _createOffer() async {
    RTC.RTCSessionDescription description = await _peerConnection!.createOffer({
      'offerToReceiveVideo': 1,
      'offerToReceiveAudio': 1,
    });
    // Completer<void> iceGatheringCompleter = Completer<void>();
    // _peerConnection!.onIceGatheringState = (RTC.RTCIceGatheringState state) {
    //   _showToast(context, "new ice gathering state: ${state}");
    //   if (state == RTC.RTCIceGatheringState.RTCIceGatheringStateComplete) {
    //     // ICE gathering complete
    //     iceGatheringCompleter.complete();
    //     _showToast(context, "new ice gathering completed");
    //   }
    // };
    // _peerConnection!.onIceCandidate = (RTCIceCandidate candidate) {
    //   if (candidate.candidate != null) {
    //     SocketEmit().sendIceCandidateBroadcast(candidate.candidate!);
    //     // rtcIceCadidates.add(candidate);
    //   }
    // };
    var session = parse(description.sdp.toString());
    String sdp = write(session, null);
    // sdp = sdp.enableAudioDTX().setPreferredCodec();
    final RTCSessionDescription descriptionSdp = RTCSessionDescription(
      sdp,
      "offer",
    );
    _peerConnection!.setLocalDescription(descriptionSdp);
    await sendSdpForBroadcast(sdp);
  }

  Future sendSdpForBroadcast(
    String sdp,
  ) async {
    await SocketEmit().sendSdpForBroadcase(sdp);
  }

  Future sendSdpOnlyReceive(
    String sdp,
    String socketId,
  ) async {
    await SocketEmit().sendSdpForReceive(sdp, socketId);
  }

  @override
  Widget build(BuildContext context) {
    // _showToast(context, "the user list${socketIdRemotes}");
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        actions: [
          ElevatedButton(
            onPressed: () {
              SocketEmit().kickAll();
            },
            child: Container(
              child: Text("kick-all"),
            ),
          )
        ],
        title: socket == null
            ? Container()
            : socket!.id == null
                ? Container()
                : Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(socket!.id!),
                  ),
      ),
      body: SizedBox(
        height: size.height,
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Stack(
                children: [
                  Container(
                    color: Colors.black,
                    width: size.width,
                    height: size.height,
                    child: socketIdRemotes.isEmpty
                        ? Container()
                        : RemoteViewCard(
                            remoteRenderer: socketIdRemotes[0]['stream'],
                          ),
                  ),
                  Positioned(
                    bottom: 20.0,
                    left: 12.0,
                    right: 0,
                    child: Container(
                      color: Colors.transparent,
                      width: size.width,
                      height: size.width * .25,
                      child: socketIdRemotes.length < 2
                          ? Container()
                          : ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: socketIdRemotes.length - 1,
                              itemBuilder: (context, index) {
                                // _showToast(context,
                                //     "the user stream is :${socketIdRemotes[index + 1]['stream']}");
                                print(
                                    "the user stream is :${socketIdRemotes[index + 1]['stream']}");
                                return Container(
                                  margin: EdgeInsets.only(right: 6.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                      color: Colors.blueAccent,
                                      width: 2.0,
                                    ),
                                  ),
                                  child:
                                      //  RTCVideoView(
                                      //   socketIdRemotes[index + 1]['stream'],
                                      //   objectFit: RTCVideoViewObjectFit
                                      //       .RTCVideoViewObjectFitCover,
                                      // ),
                                      RemoteViewCard(
                                    remoteRenderer: socketIdRemotes[index + 1]
                                        ['stream'],
                                  ),
                                );
                              },
                            ),
                    ),
                  ),
                  Positioned(
                    top: 45.0,
                    left: 15.0,
                    child: Column(
                      children: [
                        _localRenderer.textureId == null
                            ? Container(
                                height: size.width * .50,
                                width: size.width * .32,
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(6.0)),
                                  border: Border.all(
                                      color: Colors.blueAccent, width: 2.0),
                                ),
                              )
                            : FittedBox(
                                fit: BoxFit.cover,
                                child: Container(
                                  height: size.width * .50,
                                  width: size.width * .32,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(6.0)),
                                    border: Border.all(
                                        color: Colors.blueAccent, width: 2.0),
                                  ),
                                  child: Transform(
                                    transform: Matrix4.identity()..rotateY(0.0),
                                    alignment: FractionalOffset.center,
                                    child: RTCVideoView(
                                      _localRenderer,
                                      objectFit: RTCVideoViewObjectFit
                                          .RTCVideoViewObjectFitContain,
                                    ),
                                    //  Texture(
                                    //     textureId: _localRenderer.textureId!),
                                  ),
                                ),
                              ),
                        SizedBox(
                          height: 8.0,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  IconButton(
                    icon: Icon(isAudioOn ? Icons.mic : Icons.mic_off),
                    onPressed: _toggleMic,
                  ),
                  IconButton(
                    icon: const Icon(Icons.call_end),
                    iconSize: 30,
                    onPressed: endCall,
                  ),
                  IconButton(
                    icon: const Icon(Icons.cameraswitch),
                    onPressed: _switchCamera,
                  ),
                  IconButton(
                    icon: Icon(isVideoOn ? Icons.videocam : Icons.videocam_off),
                    onPressed: _toggleCamera,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _toggleMic() {
    // change status
    isAudioOn = !isAudioOn;
    // enable or disable audio track
    _localStream?.getAudioTracks().forEach((track) {
      track.enabled = isAudioOn;
    });
    setState(() {});
  }

  _toggleCamera() {
    // change status
    isVideoOn = !isVideoOn;
    _localStream?.getVideoTracks().forEach((track) {
      track.enabled = isVideoOn;
    });
    setState(() {});
  }

  void _showToast(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: CustomToast(message: message),
        padding: EdgeInsets.all(100),
        duration: Duration(seconds: 5),
      ),
    );
  }

  Future<MediaStream?> _getUserMedia() async {
    final Map<String, dynamic> mediaConstraints = {
      'audio': isAudioOn,
      'video': isVideoOn
          ? {'facingMode': _isFrontCamera ? 'user' : 'environment'}
          : false,
    };

    try {
      RTC.MediaStream stream =
          await RTC.navigator.mediaDevices.getUserMedia(mediaConstraints);

      setState(() {
        _localRenderer.srcObject = stream;
      });

      return stream;
    } catch (e) {
      print('Failed to get user media: $e');
      // Handle error, possibly show a message to the user
    }
  }

  _switchCamera() async {
    if (_localStream != null) {
      bool value = await _localStream!.getVideoTracks()[0].switchCamera();
      while (value == _isFrontCamera)
        value = await _localStream!.getVideoTracks()[0].switchCamera();
      _isFrontCamera = value;
    }
  }

  endCall() {
    socket!.disconnect();
    _peerConnection!.close();
    _localStream!.dispose();
    _localRenderer.dispose();
  }
}
