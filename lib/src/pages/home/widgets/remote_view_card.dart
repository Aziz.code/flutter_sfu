import 'package:flutter/material.dart';
import 'package:flutter_sfu2/src/pages/home/widgets/custom_toast.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart' as RTC;
import 'package:flutter_webrtc/flutter_webrtc.dart';

class RemoteViewCard extends StatefulWidget {
  final RTC.RTCVideoRenderer? remoteRenderer;
  RemoteViewCard({
    this.remoteRenderer,
  });

  @override
  State<StatefulWidget> createState() => _RemoteViewCardState();
}

class _RemoteViewCardState extends State<RemoteViewCard> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  endCall() {}

  @override
  Widget build(BuildContext context) {
    if (widget.remoteRenderer!.textureId != null) {
      print(widget.remoteRenderer!.textureId);
      // _showToast(context, widget.remoteRenderer!.textureId);
    }
    Size size = MediaQuery.of(context).size;
    return Container(
      child: widget.remoteRenderer!.textureId == null
          ? Container()
          : FittedBox(
              fit: BoxFit.cover,
              child: Container(
                height: size.width * .45,
                width: size.width * .45,
                child: Transform(
                  transform: Matrix4.identity()..rotateY(0.0),
                  alignment: FractionalOffset.center,
                  child: RTCVideoView(
                    widget.remoteRenderer!,
                    objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitCover,
                  ),
                  //  Texture(textureId: widget.remoteRenderer!.textureId!),
                ),
              ),
            ),
    );
  }
}
