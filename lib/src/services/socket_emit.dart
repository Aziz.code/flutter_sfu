import 'package:flutter_sfu2/src/pages/home/home_page.dart';

class SocketEmit {
  sendSdpForBroadcase(String sdp) {
    socket!.emit('SEND-CSS', {'sdp': sdp});
  }

  sendSdpForReceive(String sdp, String socketId) {
    socket!.emit('RECEIVE-CSS', {
      'sdp': sdp,
      'socketId': socketId,
    });
  }

  sendIceCandidate(String candidate, String socketId) {
    socket!.emit('ICE-CANDIDATE', {
      'candidate': candidate,
      'userToSignal': socketId,
    });
  }

  sendIceCandidateBroadcast(String candidate) {
    socket!.emit('ICE-CANDIDATE-BROADCAST', {'candidate': candidate});
  }

  sendIceCandidateSender(
      dynamic candidate, String socketId, String? sdpMid, int? sdpMlineIndex) {
    socket!.emit('ICE-CANDIDATE-SENDER', {
      'candidate': candidate,
      'socketId': socketId,
      'sdpMid': sdpMid,
      "sdpMLineIndex": sdpMlineIndex
    });
  }

  sendIceCandidateReceiver(
      dynamic candidate, String socketId, String? sdpMid, int? sdpMLineIndex) {
    socket!.emit('ICE-CANDIDATE-RECEIVER', {
      'candidate': candidate,
      'socketId': socketId,
      'sdpMid': sdpMid,
      "sdpMLineIndex": sdpMLineIndex
    });
  }

  kickAll() {
    socket!.emit('kick-all');
  }
}
