import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_sfu2/src/routes/app_pages.dart';
import 'package:flutter_sfu2/src/shared/logger/logger_utils.dart';

import 'package:get/get.dart';

void main() async {
  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    enableLog: true,
    logWriterCallback: Logger.write,
    initialRoute: AppPages.INITIAL,
    getPages: AppPages.routes,
  ));
}
